//https://code.google.com/codejam/contest/32016/dashboard#s=p0
public class minimumScalar {
    public static void main(String[] args) {
        int[] arr1 = {1,3,-5};
        int[] arr2 = {-2,4,1};

        java.util.Arrays.sort(arr1);
        java.util.Arrays.sort(arr2);

        int answer = 0;
        for(int i=0; i<arr1.length; i++) {
            answer += arr1[i] * arr2[arr2.length-1-i];
        }
        System.out.println("answer = " + answer);
    }
}
